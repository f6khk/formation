# Formation

Le Radioclub d'Antibes, F6KHK, organise régulièrement des sessions de cours en vue de passer la licence de radioamateur. Ils ont normalement lieu au CIV de Valbonne.

Nous utilisons le support [http://f6kgl.f5kff.free.fr/cours_radio.pdf](http://f6kgl.f5kff.free.fr/cours_radio.pdf) du Radioclub de la Haute-Ile [https://f6kgl-f5kff.fr/](https://f6kgl-f5kff.fr/). Qu'ils soient chaleureusement remerciés de leur document.


Ce dossier contient les notes de cours additionnelles et informelles que nous partageons avec les candidats qui nous font l'honneur de préparer leur license avec nous :)



Site web du club : [http://f6khk.com/](http://f6khk.com/)
